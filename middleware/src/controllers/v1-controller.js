import request from 'request';
import config from '../configs/config';
import payload from '../components/user';
import Request from 'request';

const callIntegration = (req, res) => {
    return new Promise((resolve, reject) => {
        let userObj = new payload(req.body);
        let responseObj = {
            'status': true,
            'payload': userObj.getPayload()
        }
        Request.post({
            url: config.URL,
            body: responseObj,
            json: true
        }, function (error, response, body) {
            if (error) {
                reject(error);
            }
            resolve(body);
        });

    })
}

module.exports = {
    "sayping": function (req, res) {
        if (!req.body) {
            res.status(404).json({ 'status': false });
        } else {
            res.status(200).json({ 'status': true });
        }

    },
    "createUser": (req, res) => {
        callIntegration(req, res).then((result) => {
            res.contentType('application/xml');
            res.status(200)
            .send(result);
        }).catch((error)=>{
            res.status(200).json({ 'status': false });
        })
    }
}
