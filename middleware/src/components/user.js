class payload{
    constructor(body){
        this.name = body.name || "";
        this.customerid = body.customerid || "";
        this.gender = body.gender  || "";
        this.dob = body.dob || "";
        this.accountnumber = body.accountnumber || "";
    }

     getPayload(){
        let obj = {
            "name":this.name,
            "cusotomerid":this.customerid,
            "gender":this.gender,
            "dob":this.dob,
            "accountnumber":this.accountnumber
            
        }

        return obj;
    }

   
}

export default payload;