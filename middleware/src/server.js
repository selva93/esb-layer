
import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import routes from './routes/v1-routes';

dotenv.config();


const app = new express();



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false  }));
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });
 app.use('/api', routes);

// start app
app.listen(process.env.PORT, (error) => {
    if (!error) {
        console.log(`app is running on port: ${process.env.PORT}!`); // eslint-disable-line
    }
});


