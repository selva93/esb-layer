import express from 'express';
import v1Controller from '../controllers/v1-controller';

const router = express.Router();


router.route('/ping').get(v1Controller.sayping);
router.route('/create').post(v1Controller.createUser);

module.exports = router;