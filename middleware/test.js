const request = require('request');

const getUsers = () => {
    return new Promise((resolve, reject) => {
        request.get('https://jsonplaceholder.typicode.com/users', (error, response, body) => {
            if (error) {
                reject(error);
            }
            resolve(body);
        })
    })
}


getUsers()
    .then((result) => {
        let name = JSON.parse(result);
        let nameList = [];
        name.map((q) => {
            nameList.push(q.name);
        })
    }).catch((err) => {
        console.log(err);
    })

function search(item, query) {
    return item.filter(b => {
        return b.indexOf(query) > -1;
    })
}