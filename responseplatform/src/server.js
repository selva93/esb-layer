
import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import routes from './routes/v1-routes';
import XMLBodyParser from 'express-xml-bodyparser';
import config from './configs/config';
dotenv.config();


const app = new express();


mongoose.Promise = global.Promise;
//MongoDB Connection
mongoose.connect(config.mongourl, { useMongoClient: true},function(err,database){
    if(err){
        console.log("Unable to connect",err);
    }
    console.log("Connected to database");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false  }));
app.use(XMLBodyParser())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
 app.use('/api', routes);

// start app
app.listen(process.env.PORT, (error) => {
    if (!error) {
        console.log(`app is running on port: ${process.env.PORT}!`); // eslint-disable-line
    }
});


