import express from 'express';
import v1Controller from '../controllers/v1-controller';
import XMLBodyParser from 'express-xml-bodyparser';
const router = express.Router();


router.route('/ping').get(v1Controller.sayping);
router.route('/create',XMLBodyParser({trim: false, explicitArray: false})).post(v1Controller.createUser);

module.exports = router;