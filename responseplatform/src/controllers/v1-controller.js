import request from 'request';
import config from '../configs/config';
import ResponseModel from '../models/ResponseModel';
import XML2JS from 'xml2js';
import User from '../components/user';
module.exports = {
    "sayping": function (req, res) {
        if (!req.body) {
            res.status(404).json({ 'status': false });
        } else {
            res.status(200).json({ 'status': true });
        }

    },
    "createUser": (req, res) => {
        res.contentType('application/xml');
        let requestBody = req.body.integratorlayer.messagekey[0];
        let userObj = new User(requestBody);
        let responseBody = new ResponseModel(userObj);

        if (!req.body) {
            res.status(200).json({
                'status': false
            });
        } else {
            responseBody.save((err, result) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send({
                        'status': true,
                        'response_id': result._id
                    })
                }
            })
        }
    }
}