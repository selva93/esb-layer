const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ResponseSchema = new Schema({
    name: { type: 'String', required: true },
    customerid:{type:'String',required: true,unique:true },
    gender:{type:'String',required: true,unique:true },
    dob:{type:'String',required: true },
    accountnumber:{type:'Object',required:true},
    created_date: { type: 'Date', default: Date.now, required: true }
   
  });
  
module.exports =  mongoose.model('responsemodel', ResponseSchema);
  