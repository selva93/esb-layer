import IXML from '../components/IXml';
class MessageKey{
    constructor(body){
        this.name = body.name || "";
        this.customerid = body.customerid || "";
        this.gender = body.gender || "";
        this.dob = body.dob || "";
        this.accountnumber = body.accountnumber || "";
    }
}

class Security {
    constructor(){
        this.Token = '';
        this.RealUserLoginSessionId = '';
        this.RealUser = '';
        this.RealUserPwd = '';
        this.SSOTransferToken = '';
    }
}
class RequestBody{
    constructor(payload){
        this.Session = new IXML();
        this.RequestHeader = new RequestHeader(payload);
        this.MessageKey = new MessageKey(payload);
        this.Security = new Security();
    }
}

class RequestHeader {
    constructor(payload){
        this.BankId = payload.bankId || '' ;
        this.EntityId = payload.bankId || '';
        this.EntityType = payload.bankId || ''; 
    }

}

export default RequestBody;