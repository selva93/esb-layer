

class DTD {
    constructor(){
        this.include= true;
        this.name= "empower.com";
        this.sysId= "integration.xml";
        this.pubId= "empower";

    }
   
}
class XMLOptions{
    constructor(){
        this.dtd = new DTD();
    }
}

export default XMLOptions;