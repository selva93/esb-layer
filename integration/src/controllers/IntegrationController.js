import request from 'request';
import config from '../configs/config';
import Js2XmlParser from 'js2xmlparser';
import XMLBuilder from 'xmlbuilder';
import RequestBody from '../components/RequestBody';
import XMLOptions from '../components/XMLOptions';

module.exports = {
    "sayping": function (req, res) {
        if (!req.body) {
            res.status(404).json({ 'status': false });
        } else {
            res.status(200).json({ 'status': true });
        }

    },
    "convertXML": (req, res) => {
        if (!req.body) {
            res.status(200).json({
                'status': false
            });
        } else {
            let responseBody = req.body;
           // console.log("Response body",req.body);
            let xmlOptions = new XMLOptions();
            let userElement = new RequestBody(req.body.payload);
            res.set('Content-Type', 'text/xml');
            res.status(200)
            .send(Js2XmlParser.parse('IntegratorLayer', userElement,xmlOptions));
        }
    }
}