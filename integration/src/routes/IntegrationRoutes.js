import express from 'express';
import IntegrationController from '../controllers/IntegrationController';

const router = express.Router();


router.route('/ping').get(IntegrationController.sayping);
router.route('/convertxml').post(IntegrationController.convertXML);

module.exports = router;