var userpo_Module_Factory = function () {
  var userpo = {
    name: 'userpo',
    typeInfos: [{
        localName: 'IntegratorLayer.MessageKey',
        typeName: null,
        propertyInfos: [{
            name: 'name',
            required: true,
            elementName: {
              localPart: 'name'
            }
          }, {
            name: 'customerid',
            required: true,
            elementName: {
              localPart: 'customerid'
            }
          }, {
            name: 'gender',
            required: true,
            elementName: {
              localPart: 'gender'
            }
          }, {
            name: 'dob',
            required: true,
            elementName: {
              localPart: 'dob'
            }
          }, {
            name: 'accountnumber',
            required: true,
            elementName: {
              localPart: 'accountnumber'
            },
            typeInfo: 'Int'
          }]
      }, {
        localName: 'IntegratorLayer',
        typeName: null,
        propertyInfos: [{
            name: 'session',
            required: true,
            elementName: {
              localPart: 'Session'
            },
            typeInfo: '.IntegratorLayer.Session'
          }, {
            name: 'requestHeader',
            required: true,
            elementName: {
              localPart: 'RequestHeader'
            },
            typeInfo: '.IntegratorLayer.RequestHeader'
          }, {
            name: 'messageKey',
            required: true,
            elementName: {
              localPart: 'MessageKey'
            },
            typeInfo: '.IntegratorLayer.MessageKey'
          }, {
            name: 'security',
            required: true,
            elementName: {
              localPart: 'Security'
            },
            typeInfo: '.IntegratorLayer.Security'
          }]
      }, {
        localName: 'IntegratorLayer.Security',
        typeName: null,
        propertyInfos: [{
            name: 'token',
            required: true,
            elementName: {
              localPart: 'Token'
            }
          }, {
            name: 'realUserLoginSessionId',
            required: true,
            elementName: {
              localPart: 'RealUserLoginSessionId'
            }
          }, {
            name: 'realUser',
            required: true,
            elementName: {
              localPart: 'RealUser'
            }
          }, {
            name: 'realUserPwd',
            required: true,
            elementName: {
              localPart: 'RealUserPwd'
            }
          }, {
            name: 'ssoTransferToken',
            required: true,
            elementName: {
              localPart: 'SSOTransferToken'
            }
          }]
      }, {
        localName: 'IntegratorLayer.Session',
        typeName: null,
        propertyInfos: [{
            name: 'globaluuid',
            required: true,
            elementName: {
              localPart: 'globaluuid'
            }
          }, {
            name: 'serviceid',
            required: true,
            elementName: {
              localPart: 'serviceid'
            }
          }, {
            name: 'serviceversion',
            required: true,
            elementName: {
              localPart: 'serviceversion'
            },
            typeInfo: 'Float'
          }]
      }, {
        localName: 'IntegratorLayer.RequestHeader',
        typeName: null,
        propertyInfos: [{
            name: 'bankId',
            required: true,
            elementName: {
              localPart: 'BankId'
            }
          }, {
            name: 'entityId',
            required: true,
            elementName: {
              localPart: 'EntityId'
            }
          }, {
            name: 'entityType',
            required: true,
            elementName: {
              localPart: 'EntityType'
            }
          }]
      }],
    elementInfos: [{
        elementName: {
          localPart: 'IntegratorLayer'
        },
        typeInfo: '.IntegratorLayer'
      }]
  };
  return {
    userpo: userpo
  };
};
if (typeof define === 'function' && define.amd) {
  define([], userpo_Module_Factory);
}
else {
  var userpo_Module = userpo_Module_Factory();
  if (typeof module !== 'undefined' && module.exports) {
    module.exports.userpo = userpo_Module.userpo;
  }
  else {
    var userpo = userpo_Module.userpo;
  }
}